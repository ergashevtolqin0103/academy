<?php

namespace Database\Seeders;

use App\Models\AcademyCategoryBall;
use App\Models\AcademySubject;
use App\Models\AcademyTeacher;
use App\Models\AcademyUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $first = AcademyUser::create([
            'first_name' => 'Muhlisa',
            'last_name' => 'Hakimova',
            'phone' => '+998977741027',
            'birthday' => '1995-06-06',
            'status' => 3,
            'parol' => '1995',
            'username' => 'nvt1995',
            'password' => Hash ::make('1995'),
        ]);

        $second = AcademyUser::create([
            'first_name' => 'Botir',
            'last_name' => 'Baydullayev',
            'phone' => '+998330616155',
            'birthday' => '1996-06-06',
            'status' => 3,
            'parol' => '1996',
            'username' => 'nvt1996',
            'password' => Hash ::make('1996'),
        ]);

        $third = AcademyUser::create([
            'first_name' => 'To\'lqin',
            'last_name' => 'Ergashev',
            'phone' => '+998990821015',
            'birthday' => '1999-03-01',
            'status' => 3,
            'parol' => '1999',
            'username' => 'nvt1999',
            'password' => Hash ::make('1999'),
        ]);

        $techer_id = [$second->id,$first->id,$third->id];
        $subjects = ['Bilim','Sotuv','Jang'];

        foreach($subjects as $t => $key)
        {
            $subject = AcademySubject::create([
                'name' => $key,
            ]);

            AcademyTeacher::create([
                'user_id' => $techer_id[$t],
                'subject_id' => $subject->id,
            ]);
        }

        $category = ['Bilim','Sotuv','Jang','Intizom','Aktiv','Savol-javob','Bot','Shou'];

        foreach($category as $t => $key)
        {
            AcademyCategoryBall::create([
                'name' => $key,
            ]);
        }
        
    }
}
