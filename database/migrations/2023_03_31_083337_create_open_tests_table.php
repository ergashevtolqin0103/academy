<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpenTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academy_open_tests', function (Blueprint $table) {
            $table->id();
            $table->longText('question');
            $table->longText('answer_a');
            $table->longText('answer_b');
            $table->longText('answer_c');
            $table->longText('answer_d');
            $table->string('answer');
            $table->foreignId('lesson_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_tests');
    }
}
