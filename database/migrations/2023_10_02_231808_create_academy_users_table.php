<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academy_users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->date('birthday');
            $table->foreignId('region_id')->nullable();
            $table->foreignId('district_id')->nullable();
            $table->foreignId('teacher_id')->nullable();
            $table->foreignId('rekrut_id')->nullable();
            $table->foreignId('rol_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->integer('status')->default(1);
            $table->string('image')->nullable();
            $table->integer('parol');
            $table->string('username');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academy_users');
    }
}
