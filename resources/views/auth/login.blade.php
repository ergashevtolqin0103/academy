<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('partials.css')
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  
        bg-beige-1
        ">


      <div class="content-wrapper  js-content-wrapper">

        <section class="form-page js-mouse-move-container">
          <div class="form-page__img bg-dark-1">
            <div class="form-page-composition">
              <div class="-bg"><img data-move="30" class="js-mouse-move" src="{{asset('assets/img/login/bg.png')}}" alt="bg"></div>
              <div class="-el-1"><img data-move="20" class="js-mouse-move" src="{{asset('assets/img/home-9/hero/bg.png')}}" alt="image"></div>
              {{-- <div class="-el-2"><img data-move="40" class="js-mouse-move" src="{{asset('assets/img/home-9/hero/1.png')}}" alt="icon"></div> --}}
              {{-- <div class="-el-3"><img data-move="40" class="js-mouse-move" src="{{asset('assets/img/home-9/hero/2.png')}}" alt="icon"></div> --}}
              {{-- <div class="-el-4"><img data-move="40" class="js-mouse-move" src="{{asset('assets/img/home-9/hero/3.png')}}" alt="icon"></div> --}}
            </div>
          </div>

          <div class="form-page__content lg:py-50">
            <div class="container">
              <div class="row justify-center items-center">
                <div class="col-xl-6 col-lg-8">
                  <div class="px-50 py-50 md:px-25 md:py-25 bg-white shadow-1 rounded-16">
                    {{-- <h3 class="text-30 lh-13">Login</h3> --}}
                    {{-- <p class="mt-10">Don't have an account yet? <a href="signup.html" class="text-purple-1">Sign up for free</a></p> --}}

                    <form class="contact-form respondForm__form row y-gap-20 pt-30" action="{{route('login')}}" method="POST">
                        @csrf
                      <div class="col-12">
                        <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Username</label>
                        <input type="text" name="username">
                      </div>
                      <div class="col-12">
                        <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Parol</label>
                        <input type="password" name="password">
                      </div>
                      <div class="col-12">
                        <button type="submit" name="submit" id="submit" class="button -md -green-1 text-dark-1 fw-500 w-1/1">
                          Kirish
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


      </div>
    </main>
  </div>
  @include('partials.js')
</body>

</html>