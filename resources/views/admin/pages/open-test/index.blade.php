@extends('admin.layouts.app')
@section('admin-content')
    <div class="dashboard__content bg-light-4 pt-1">

        <div class="row y-gap-30">
            <div class="col-12">
                <div class="rounded-16 bg-white shadow-4 h-100">
                    {{-- <div class="d-flex items-center py-20 px-30 border-bottom-light">
              <h2 class="text-17 lh-1 fw-500">Page Head</h2>
            </div> --}}
                    <div class="d-flex justify-between items-center py-20 px-30 border-bottom-light">
                        <h2 class="text-17 lh-1 fw-500">Testlar ro'yhati</h2>
                        <div>
                            <a href="{{ route('open-test.create') }}" class="button h-50 px-30 -purple-1 text-white">Yangi
                                test qo'shish</a>
                        </div>
                    </div>
                    <div class="py-30 px-30">
                        <div class="col-lg-12">
                            <table class="table w-1/1">
                                <thead>
                                    <tr>
                                        <th>Savol</th>
                                        <th>Variant A</th>
                                        <th>Variant B</th>
                                        <th>Variant C</th>
                                        <th>Variant D</th>
                                        <th>Javob</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @isset($open_tests)
                                      @foreach ($open_tests as $item)
                                      <tr>
                                        <td>{{$item->question}}</td>
                                        <td>{{$item->answer_a}}</td>
                                        <td>{{$item->answer_b}}</td>
                                        <td>{{$item->answer_c}}</td>
                                        <td>{{$item->answer_d}}</td>
                                        <td>{{$item->answer}}</td>
                                        <td>
                                          <div class="d-flex x-gap-10 items-center">
                                            <a href="{{route('open-test.edit',$item->id)}}" class="icon icon-edit mr-5" style="color:blue"></a>
                                            {{-- <a href="#" class="icon icon-bin" style="color:rgb(227, 11, 11)"></a> --}}
                                          </div>
                                        </td>
                                        
                                      </tr>
                                      @endforeach
                                  @endisset

                                </tbody>
                            </table>
                        </div>
                        
                        {{-- @if ($open_tests->lastPage() > 1)
                            <ul class="pagination">
                                <li class="{{ ($open_tests->currentPage() == 1) ? ' disabled' : '' }}">
                                    <a href="{{ $open_tests->url(1) }}">First</a>
                                </li>
                                @for ($i = 1; $i <= $open_tests->lastPage(); $i++)
                                    <?php
                                    $half_total_links = floor($link_limit / 2);
                                    $from = $open_tests->currentPage() - $half_total_links;
                                    $to = $open_tests->currentPage() + $half_total_links;
                                    if ($open_tests->currentPage() < $half_total_links) {
                                      $to += $half_total_links - $open_tests->currentPage();
                                    }
                                    if ($open_tests->lastPage() - $open_tests->currentPage() < $half_total_links) {
                                        $from -= $half_total_links - ($open_tests->lastPage() - $open_tests->currentPage()) - 1;
                                    }
                                    ?>
                                    @if ($from < $i && $i < $to)
                                        <li class="{{ ($open_tests->currentPage() == $i) ? ' active' : '' }}">
                                            <a href="{{ $open_tests->url($i) }}">{{ $i }}</a>
                                        </li>
                                    @endif
                                @endfor
                                <li class="{{ ($open_tests->currentPage() == $open_tests->lastPage()) ? ' disabled' : '' }}">
                                    <a href="{{ $open_tests->url($open_tests->lastPage()) }}">Last</a>
                                </li>
                            </ul>
                        @endif --}}

                        @include('components.paginate',['open_tests' => $open_tests])

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('admin-script')
@endsection
