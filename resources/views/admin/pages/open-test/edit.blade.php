@extends('admin.layouts.app')
@section('admin-content')
    <div class="dashboard__content bg-light-4 pt-1">

        <div class="row x-gap-60 y-gap-30 mt-10">
            @if ($message = Session::get('save_success'))
              <div class="col-12 alerts-all" style="padding-top: 5px;padding-bottom: 0px;">
                <div class="d-flex items-center justify-between bg-success-1 pl-30 pr-20 py-10 rounded-8">
                    <div class="text-success-2 lh-1 fw-500">{{ $message }}</div>
                    {{-- <div class="text-success-2 size-20" data-feather="x"></div> --}}
                </div>
              </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="col-12 alerts-all" style="padding-top: 5px;padding-bottom: 0px;">
                        <div class="d-flex items-center justify-between bg-error-1 pl-30 pr-20 py-10 rounded-8">
                            <div class="text-error-2 lh-1 fw-500">{{ $error }}</div>
                            {{-- <div class="text-error-2 size-20" data-feather="x"></div> --}}
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="d-flex justify-between items-center py-20 px-30 border-bottom-light">
              <h2 class="text-17 lh-1 fw-500">Testni o'zgartirish</h2>
                <div>
                  <a href="{{route('open-test.index')}}" class="button h-50 px-30 -purple-1 text-white">Testlar ro'yhati</a>
                </div>
            </div>
            <form action="{{ route('open-test.update',$open_test->id) }}" method="POST" class="contact-form">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-12">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Test savali</label>
                            <textarea rows="3" name="question">{{$open_test->question}}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="col-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Javob A</label>
                          <textarea rows="2" name="answer_a">{{$open_test->answer_a}}</textarea>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="col-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Javob B</label>
                          <textarea rows="2" name="answer_b">{{$open_test->answer_b}}</textarea>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="col-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Javob C</label>
                          <textarea rows="2" name="answer_c">{{$open_test->answer_c}}</textarea>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="col-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Javob D</label>
                          <textarea rows="2" name="answer_d">{{$open_test->answer_d}}</textarea>
                      </div>
                    </div>
                    <div class="col-lg-6">
                        <label class="text-16 lh-1 fw-300 text-dark-1 mb-10">Test javobi</label>
                        <div class="form-select">
                          @php
                              $arr = ['A','B','C','D'];
                          @endphp
                            <select class="selectize wide js-selectize" name="answer">
                                @foreach ($arr as $item)
                                  @if ($open_test->answer == $item)
                                    <option value="{{$item}}" style="padding: 0px 24px;" selected>{{$item}}</option>
                                  @else
                                    <option value="{{$item}}" style="padding: 0px 24px;" selected>{{$item}}</option>
                                  @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-select mt-35">
                          <button type="submit" class="button -icon -outline-purple-1 text-purple-1"
                              style="width: 100%;">
                              O'zgarishni saqlash
                          </button>
                        </div>
                    </div>
              </div>

            </form>
        </div>

    </div>
@endsection
@section('admin-script')
<script>
  $(document).ready(function() {
    $(".alerts-all").fadeTo(2000, 500).slideUp(2000, function() {
      $(".alerts-all").slideUp(2000);
    });
  });
</script>
@endsection