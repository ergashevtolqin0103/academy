
@extends('admin.layouts.app')
@section('admin-content')
    <div class="dashboard__content bg-light-4 pt-1">

        <div class="row y-gap-30">
            <div class="col-12">
                <div class="rounded-16 bg-white shadow-4 h-100">
                    {{-- <div class="d-flex items-center py-20 px-30 border-bottom-light">
              <h2 class="text-17 lh-1 fw-500">Page Head</h2>
            </div> --}}
                    <div class="d-flex justify-between items-center py-20 px-30 border-bottom-light">
                        <h2 class="text-17 lh-1 fw-500">Nomzodlar</h2>
                    </div>
                    <div class="py-30 px-30">
                        <div class="col-lg-12">
                            <table class="table w-1/1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>FIO</th>
                                        <th>Telefon</th>
                                        <th>Birthday</th>
                                        <th>Viloyat</th>
                                        <th>Tuman</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @isset($users)
                                      @foreach ($users as $key => $item)
                                      <form action="{{route('nomzod.ball',$item->id)}}" method="post">
                                        @csrf
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$item->rekrut->full_name}} {{$item->rekrut->last_name}}</td>
                                            <td>{{$item->rekrut->phone}}</td>
                                            <td>{{$item->birthday}}</td>
                                            <td>{{$item->rekrut->region->name}}</td>
                                            <td>{{$item->rekrut->district->name}}</td>
                                            <td>
                                                <select name="xolat" id="" class="form-control">
                                                    <option value="" selected disabled>Xolatni tanlang</option>
                                                    @foreach ($category as $item)
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                </select>

                                                <input type="integer" name="ball">
                                                <button type="submit" class="btn">
                                                    <i class="fas fa-save"></i>
                                                </button>
                                            </td>
                                            
                                        </tr>
                                      </form>
                                      @endforeach
                                  @endisset

                                </tbody>
                            </table>
                        </div>                                   

                        {{-- @include('components.paginate',['pagination_v' => $lessons]) --}}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('admin-script')
@endsection
