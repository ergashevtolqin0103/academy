
@extends('admin.layouts.app')
@section('admin-content')
    <div class="dashboard__content bg-light-4 pt-1">

        <div class="row y-gap-30">
            <div class="col-12">
                <div class="rounded-16 bg-white shadow-4 h-100">
                    {{-- <div class="d-flex items-center py-20 px-30 border-bottom-light">
              <h2 class="text-17 lh-1 fw-500">Page Head</h2>
            </div> --}}
                    <div class="d-flex justify-between items-center py-20 px-30 border-bottom-light">
                        <h2 class="text-17 lh-1 fw-500">Testlar ro'yhati</h2>
                        <div>
                            <a href="{{ route('lesson.create') }}" class="button h-50 px-30 -purple-1 text-white">Yangi
                                test qo'shish</a>
                        </div>
                    </div>
                    <div class="py-30 px-30">
                        <div class="col-lg-12">
                            <table class="table w-1/1">
                                <thead>
                                    <tr>
                                        <th>Nomi</th>
                                        <th>Kurs</th>
                                        <th>Izoh</th>
                                        <th>Link</th>
                                        <th>Tartib raqami</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @isset($lessons)
                                      @foreach ($lessons as $item)
                                      <tr>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->course->title}}</td>
                                        <td>{{$item->description}}</td>
                                        <td>{{$item->video_link}}</td>
                                        <td>{{$item->sort}}</td>
                                        
                                      </tr>
                                      @endforeach
                                  @endisset

                                </tbody>
                            </table>
                        </div>                                   

                        @include('components.paginate',['pagination_v' => $lessons])

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('admin-script')
@endsection
