
@extends('admin.layouts.app')
@section('admin-content')
    <div class="dashboard__content bg-light-4 pt-1">

        <div class="row x-gap-60 y-gap-30 mt-10">
            @if ($message = Session::get('save_success'))
              <div class="col-12 alerts-all" style="padding-top: 5px;padding-bottom: 0px;">
                <div class="d-flex items-center justify-between bg-success-1 pl-30 pr-20 py-10 rounded-8">
                    <div class="text-success-2 lh-1 fw-500">{{ $message }}</div>
                </div>
              </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="col-12 alerts-all" style="padding-top: 5px;padding-bottom: 0px;">
                        <div class="d-flex items-center justify-between bg-error-1 pl-30 pr-20 py-10 rounded-8">
                            <div class="text-error-2 lh-1 fw-500">{{ $error }}</div>
                            {{-- <div class="text-error-2 size-20" data-feather="x"></div> --}}
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="d-flex justify-between items-center py-20 px-30 border-bottom-light">
                <h2 class="text-17 lh-1 fw-500">Dars qo'shish</h2>
                  <div>
                    <a href="{{route('lesson.index')}}" class="button h-50 px-30 -purple-1 text-white">Darslar ro'yhati</a>
                  </div>
              </div>
            <form action="{{ route('lesson.store') }}" method="POST" class="contact-form">
                @csrf
                <div class="row">
                    <div class="col-lg-12 mb-2">
                      <div class="col-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Dars nomi</label>
                          <input type="text" name="title" placeholder="Dars nomini kiriting...">
                      </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-12">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Dars izoh</label>
                            <textarea placeholder="Izohni kiriting..." rows="3" name="description"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="col-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Video linki</label>
                          <input type="text" name="video_link" placeholder="Video linkini kiriting...">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="col-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Tartib raqami</label>
                          <input type="text" name="sort" placeholder="Tartib raqamini kiriting...">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Kursni tanlang</label>
                        <div class="form-select">
                            <select class="selectize wide js-selectize" name="course_id">
                                <option selected disabled></option>
                                @foreach ($courses as $item)
                                  <option value="{{$item->id}}" style="padding: 0px 24px;">{{$item->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-select mt-35">
                          <button type="submit" class="button -icon -outline-purple-1 text-purple-1"
                              style="width: 100%;">
                              Saqlash
                          </button>
                        </div>
                    </div>
              </div>

            </form>
        </div>

    </div>
@endsection
@section('admin-script')
<script>
  $(document).ready(function() {
    $(".alerts-all").fadeTo(2000, 500).slideUp(2000, function() {
      $(".alerts-all").slideUp(2000);
    });
  });
</script>
@endsection