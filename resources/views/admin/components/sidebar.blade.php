<div class="dashboard__sidebar scroll-bar-1">
  
  
              <div class="sidebar -dashboard">
  
                {{-- <div class="sidebar__item ">
                  <a href="dashboard.html" class="d-flex items-center text-17 lh-1 fw-500 ">
                    <i class="text-20 icon-discovery mr-15"></i>
                    Testlar
                  </a>
                </div> --}}

                @if (Auth::user()->parol == 1999)
                    

                <div>

                  <div class="accordion -block js-accordion">

                    <div class="accordion__item bg-light-4">
                      <div class="accordion__button">
                      <i class="text-20 icon-discovery mr-15"></i>
                        <span class="text-17 fw-500 text-dark-1">Kurs</span>
                      </div>

                      <div class="accordion__content">
                          <div class="sidebar__item ml-15">
                            <a href="{{route('course.create')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Kurs qo'shish
                            </a>
                          </div>
                          <div class="sidebar__item ml-15">
                            <a href="{{route('course.index')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Kurs ro'yhati
                            </a>
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="accordion -block js-accordion">

                    <div class="accordion__item bg-light-4">
                      <div class="accordion__button">
                      <i class="text-20 icon-discovery mr-15"></i>
                        <span class="text-17 fw-500 text-dark-1">Dars</span>
                      </div>

                      <div class="accordion__content">
                          <div class="sidebar__item ml-15">
                            <a href="{{route('lesson.create')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Dars qo'shish
                            </a>
                          </div>
                          <div class="sidebar__item ml-15">
                            <a href="{{route('lesson.index')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Darslar ro'yhati
                            </a>
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="accordion -block js-accordion">

                    <div class="accordion__item bg-light-4">
                      <div class="accordion__button">
                      <i class="text-20 icon-discovery mr-15"></i>
                        <span class="text-17 fw-500 text-dark-1">Test</span>
                      </div>

                      <div class="accordion__content">
                          <div class="sidebar__item ml-15">
                            <a href="{{route('open-test.create')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Test qo'shish
                            </a>
                          </div>
                          <div class="sidebar__item ml-15">
                            <a href="{{route('open-test.index')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Testlar ro'yhati
                            </a>
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="accordion -block js-accordion">

                    <div class="accordion__item bg-light-4">
                      <div class="accordion__button">
                      <i class="text-20 icon-discovery mr-15"></i>
                        <span class="text-17 fw-500 text-dark-1">Ustoz</span>
                      </div>

                      <div class="accordion__content">
                          <div class="sidebar__item ml-15">
                            <a href="{{route('teacher.create')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Ustoz qo'shish
                            </a>
                          </div>
                          <div class="sidebar__item ml-15">
                            <a href="{{route('teacher.index')}}" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Ustozlar ro'yhati
                            </a>
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="accordion -block js-accordion mt-5">

                    <div class="accordion__item bg-light-4" style="background: rgb(157, 184, 241)">
                      <div class="accordion__button">
                      <i class="text-20 icon-discovery mr-15"></i>
                        <span class="text-17 fw-500 text-white-1" style="color:rgb(0, 0, 0)">Nomzod</span>
                      </div>

                      <div class="accordion__content">
                          <div class="sidebar__item ml-15">
                            <a href="{{route('nomzod.list')}}" class="d-flex items-center text-17 lh-1 fw-500 text-white-1" style="color:rgb(0, 0, 0)">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Nomzodlar ro'yhati
                            </a>
                          </div>
                          <div class="sidebar__item ml-15">
                            <a href="{{route('nomzod.reg')}}" class="d-flex items-center text-17 lh-1 fw-500 text-white-1" style="color:rgb(0, 0, 0)">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Nomzodlar registratsiya
                            </a>
                          </div>
                          <div class="sidebar__item ml-15">
                            <a href="{{route('teacher.index')}}" class="d-flex items-center text-17 lh-1 fw-500 text-white-1" style="color:rgb(0, 0, 0)">
                              <i class="text-20 icon-discovery mr-15"></i>
                              Ustozlar ro'yhati
                            </a>
                          </div>
                      </div>
                    </div>
                  </div>

                </div>

                @endif
  
                <div class="sidebar__item ">
                  <a href="dshb-bookmarks.html" class="d-flex items-center text-17 lh-1 fw-500 ">
                    <i class="text-20 icon-bookmark mr-15"></i>
                    Nomzodlar
                  </a>
                </div>
                <div class="sidebar__item ">
                  <a href="#" class="d-flex items-center text-17 lh-1 fw-500 ">
                    <i class="text-20 icon-bookmark mr-15"></i>
                    Imtihon
                  </a>
                </div>
  
                
                {{-- <div class="sidebar__item -is-active -dark-bg-dark-2">
                  <a href="dshb-listing.html" class="d-flex items-center text-17 lh-1 fw-500 -dark-text-white">
                    <i class="text-20 icon-list mr-15"></i>
                    Create Course
                  </a>
                </div> --}}
  
                
  
                {{-- <div class="sidebar__item ">
                  <a href="#" class="d-flex items-center text-17 lh-1 fw-500 ">
                    <i class="text-20 icon-power mr-15"></i>
                    Logout
                  </a>
                </div> --}}
  
              </div>
  
  
            </div>