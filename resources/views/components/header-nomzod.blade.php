<header data-anim="fade" data-add-bg="" class="header -type-4 -shadow bg-white js-header">
  
  
    <div class="header__container border-bottom-light py-10">
      <div class="row justify-between items-center">

        <div class="col-auto">
          <div class="header-left d-flex items-center">

            <div class="header__logo pr-30 xl:pr-20 md:pr-0">
              <a data-barba href="/">
                <img src="{{asset('assets/img/general/logo.svg')}}" alt="logo">
              </a>
            </div>


            


            <div class="header-menu js-mobile-menu-toggle pl-30 xl:pl-20">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-35 py-20 border-bottom-light">
                  <a href="{{route('lessons')}}" class="text-dark-1">
                    <span class="material-symbols-outlined">
                      account_circle
                      </span>
                  </a>
                  {{-- <a href="signup.html" class="text-dark-1">
                    <span class="material-symbols-outlined">
                      logout
                      </span>
                  </a> --}}
                  
                </div>
                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  <ul class="menu__nav text-dark-1 -is-active">

                    {{-- <li class="bgli">
                      <a data-barba href="contact-1.html">Ustoz</a>
                    </li>
                    <li>
                      <a data-barba href="contact-1.html">Dars</a>
                    </li> --}}
                    {{-- <li> --}}
                      {{-- <a data-barba href="" data-el-toggle=".js-search-toggle">Reyting</a> --}}
                      {{-- <button class="d-flex items-center text-dark-1" data-el-toggle=".js-search-toggle">
                        Reyting
                      </button> --}}
  
                      {{-- <div class="toggle-element js-search-toggle">
                        <div class="header-search pt-90 bg-white shadow-4">
                          <div class="container">
                            <div class="header-search__field">
                              <div class="icon icon-search text-dark-1"></div>
                              <input type="text" class="col-12 text-18 lh-12 text-dark-1 fw-500" placeholder="What do you want to learn?">
  
                              <button class="d-flex items-center justify-center size-40 rounded-full bg-purple-3" data-el-toggle=".js-search-toggle">
                                <img src="{{asset('assets/img/menus/close.svg')}}" alt="icon">
                              </button>
                            </div>
  
                            <div class="header-search__content mt-30">
                              <div class="text-17 text-dark-1 fw-500">Popular Right Now</div>
  
                              <div class="d-flex y-gap-5 flex-column mt-20">
                                <a href="courses-single-1.html" class="text-dark-1">The Ultimate Drawing Course - Beginner to Advanced</a>
                                <a href="courses-single-2.html" class="text-dark-1">Character Art School: Complete Character Drawing Course</a>
                                <a href="courses-single-3.html" class="text-dark-1">Complete Blender Creator: Learn 3D Modelling for Beginners</a>
                                <a href="courses-single-4.html" class="text-dark-1">User Experience Design Essentials - Adobe XD UI UX Design</a>
                                <a href="courses-single-5.html" class="text-dark-1">Graphic Design Masterclass - Learn GREAT Design</a>
                                <a href="courses-single-6.html" class="text-dark-1">Adobe Photoshop CC – Essentials Training Course</a>
                              </div>
  
                              <div class="mt-30">
                                <button class="uppercase underline">PRESS ENTER TO SEE ALL SEARCH RESULTS</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="header-search__bg" data-el-toggle=".js-search-toggle"></div>
                      </div> --}}

                    {{-- </li> --}}
                    <li>
                      <a data-barba href="#">Imtihon</a>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                  <div class="icon-close text-dark-1 text-16"></div>
                </div>
              </div>

              <div class="header-menu-bg"></div>
            </div>

          </div>
        </div>


        <div class="col-auto">
          <div class="header-right d-flex items-center">
            <div class="header-right__icons text-white d-flex items-center">
              {{-- <div class="header-search-field">
                <form action="#">
                  <div class="header-search-field__group">
                    <input type="text" placeholder="What do you want to learn?">
                    <button type="submit">
                      <i class="icon icon-search"></i>
                    </button>
                  </div>
                </form>
              </div> --}}


              {{-- <div class="relative -after-border pl-20 sm:pl-15">
                <button class="d-flex items-center text-dark-1" data-el-toggle=".js-cart-toggle">
                  <i class="text-20 icon icon-basket"></i>
                </button>

              </div> --}}


              <div class="d-none xl:d-block pl-20 sm:pl-15">
                <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                  <i class="text-11 icon icon-mobile-menu"></i>
                </button>
              </div>

            </div>

            <div class="header-right__buttons d-flex items-center ml-30 xl:ml-20 lg:d-none">
              {{-- <a href="{{route('lessons')}}" class="button h-50 px-30 -rounded text-dark ml-20">Dars</a> --}}
              {{-- <a href="{{route('answers')}}" class="button h-50 px-30 -rounded text-dark ml-20">Ustoz</a>/ --}}
              {{-- <button class="button h-50 px-30 -rounded text-dark ml-20" data-el-toggle=".js-search-toggle">Reyting</button> --}}
              
              <a href="#" class="button h-50 px-30 -rounded text-dark ml-20">Imtihon</a>
              {{-- <a href="{{route('logout')}}" class="button h-50 px-30 -rounded text-dark ml-20">
                <span class="material-symbols-outlined">
                  logout
                  </span>
              </a> --}}
            </div>
            
          </div>
        </div>

      </div>
    </div>


  </header>
