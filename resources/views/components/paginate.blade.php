@php
    $link_limit = count($pagination_v); 
@endphp
@if ($pagination_v->lastPage() > 1)

    <div class="row justify-center pt-30 lg:pt-50">
        <div class="col-auto">
            <div class="pagination -buttons">

                <a href="{{ $pagination_v->url(1) }}">
                    <button class="pagination__button -prev">
                        <i class="icon icon-chevron-left"></i>
                    </button>
                </a>

                <div class="pagination__count">
                    @for ($i = 1; $i <= $pagination_v->lastPage(); $i++)
                        <?php
                        $half_total_links = floor($link_limit / 2);
                        $from = $pagination_v->currentPage() - $half_total_links;
                        $to = $pagination_v->currentPage() + $half_total_links;
                        if ($pagination_v->currentPage() < $half_total_links) {
                            $to += $half_total_links - $pagination_v->currentPage();
                        }
                        if ($pagination_v->lastPage() - $pagination_v->currentPage() < $half_total_links) {
                            $from -= $half_total_links - ($pagination_v->lastPage() - $pagination_v->currentPage()) - 1;
                        }
                        ?>
                        @if ($from < $i && $i < $to)
                            <a href="{{ $pagination_v->url($i) }}"
                                class="{{ $pagination_v->currentPage() == $i ? ' -count-is-active' : '' }}"
                                href="#">{{ $i }}</a>
                        @endif
                    @endfor
                </div>

                <a href="{{ $pagination_v->url($pagination_v->lastPage()) }}">
                    <button class="pagination__button -next">
                        <i class="icon icon-chevron-right"></i>
                    </button>
                </a>
            </div>
        </div>
    </div>

@endif
