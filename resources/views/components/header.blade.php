<header data-anim="fade" data-add-bg="" class="header -type-4 -shadow bg-white js-header">
  
  
    <div class="header__container border-bottom-light py-10">
      <div class="row justify-between items-center">

        <div class="col-auto">
          <div class="header-left d-flex items-center">

            <div class="header__logo pr-30 xl:pr-20 md:pr-0">
              <a data-barba href="/">
                <img src="{{asset('assets/img/general/logo.svg')}}" alt="logo">
              </a>
            </div>


            


            <div class="header-menu js-mobile-menu-toggle pl-30 xl:pl-20">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  <a href="{{route('lessons')}}" class="text-dark-1">Darslar</a>
                  <a href="{{route('lessons')}}" class="text-dark-1 ml-30">Natijalar</a>
                  {{-- <a href="signup.html" class="text-dark-1 ml-30">
                    <span class="material-symbols-outlined">
                      logout
                      </span>
                  </a> --}}
                </div>
              </div>

              <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                  <div class="icon-close text-dark-1 text-16"></div>
                </div>
              </div>

              <div class="header-menu-bg"></div>
            </div>

          </div>
        </div>


        <div class="col-auto">
          <div class="header-right d-flex items-center">
            <div class="header-right__icons text-white d-flex items-center">
              {{-- <div class="header-search-field">
                <form action="#">
                  <div class="header-search-field__group">
                    <input type="text" placeholder="What do you want to learn?">
                    <button type="submit">
                      <i class="icon icon-search"></i>
                    </button>
                  </div>
                </form>
              </div> --}}


              {{-- <div class="relative -after-border pl-20 sm:pl-15">
                <button class="d-flex items-center text-dark-1" data-el-toggle=".js-cart-toggle">
                  <i class="text-20 icon icon-basket"></i>
                </button>

              </div> --}}


              <div class="d-none xl:d-block pl-20 sm:pl-15">
                <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                  <i class="text-11 icon icon-mobile-menu"></i>
                </button>
              </div>

            </div>

            <div class="header-right__buttons d-flex items-center ml-30 xl:ml-20 lg:d-none">
              <a href="{{route('lessons')}}" class="button h-50 px-30 -rounded text-dark ml-20">Darslar</a>
              <a href="{{route('answers')}}" class="button h-50 px-30 -rounded text-dark ml-20">Natijalar</a>
              {{-- <a href="{{route('logout')}}" class="button h-50 px-30 -rounded text-dark ml-20">
                <span class="material-symbols-outlined">
                  logout
                  </span>
              </a> --}}
            </div>
          </div>
        </div>

      </div>
    </div>
  </header>
