@extends('layouts.app')
@section('content')
<section data-anim-wrap class="masthead -type-4">
    <div class="container">
      <div class="row y-gap-50 justify-center items-center">
        <div class="col-xl-5 col-lg-6">
          <div class="masthead__content">
            <h1 data-anim-child="slide-up delay-3" class="masthead__title">
              Ishingizni ustasi bo'lish uchun<br>
              <span class="text-purple-1 underline">Bilimingizni</span> oshiring 
            </h1>
            {{-- <p data-anim-child="slide-up delay-4" class="masthead__text pt-15">
              Free online courses from the world’s leading experts.<br class="md:d-none">
              Join 17 million learners today
            </p> --}}
            {{-- <div data-anim-child="slide-up delay-5" class="masthead__button row x-gap-20 y-gap-20 pt-30">
              <div class="col-auto"><a href="{{route('lessons')}}" class="button -md -purple-1 -rounded text-white">Boshlash</a></div>
            </div> --}}
          </div>
        </div>

        <div class="col-xl-6 col-lg-6">
          <div data-anim-child="slide-up delay-6" class="masthead__image">
            <img src="{{asset('assets/img/home-5/masthead/bg.svg')}}" alt="image">
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="layout-pt-lg layout-pb-lg">
    <div data-anim-wrap class="container">
      <div class="row justify-center text-center">
        <div class="col-auto">

          <div class="sectionTitle ">

            <h2 class="sectionTitle__title ">DARSLAR</h2>


          </div>

        </div>
      </div>

      <div class="row y-gap-30 justify-between pt-60 lg:pt-50">

        <div class="col-lg-3 col-md-6">
          <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 bg-white rounded-8">
            <div class="coursesCard__image">
              <img src="{{asset('assets/img/home-5/learning/1.svg')}}" alt="image">
            </div>
            <div class="coursesCard__content mt-30">
              <h5 class="coursesCard__title text-18 lh-1 fw-500">Bilim</h5>
              <p class="coursesCard__text text-14 mt-10">Bilimingizni oshirib tajribali sotuvchiga aylaning.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6">
          <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 bg-white rounded-8">
            <div class="coursesCard__image">
              <img src="{{asset('assets/img/home-5/learning/3.svg')}}" alt="image">
            </div>
            <div class="coursesCard__content mt-30">
              <h5 class="coursesCard__title text-18 lh-1 fw-500">Sotuv</h5>
              <p class="coursesCard__text text-14 mt-10">Sotuv ko'nikmalaringizni yaxshilab mijozlarni ko'paytiring.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6">
          <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 bg-white rounded-8">
            <div class="coursesCard__image">
              <img src="{{asset('assets/img/home-5/learning/4.svg')}}" alt="image">
            </div>
            <div class="coursesCard__content mt-30">
              <h5 class="coursesCard__title text-18 lh-1 fw-500">Jang</h5>
              <p class="coursesCard__text text-14 mt-10">Jangga qatnashing sovg'a va bonuslarni qo'lga kiriting.</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section class="layout-pt-lg layout-pb-lg bg-dark-5">
    <div class="container">
      <div class="row justify-center text-center">
        <div class="col-auto">

          <div class="sectionTitle ">

            <h2 class="sectionTitle__title text-white">Bizning Ustozlar</h2>

          </div>

        </div>
      </div>

      <div class="pt-60 lg:pt-50 js-section-slider" data-gap="30" data-pagination data-slider-cols="xl-2" data-anim-wrap>
        <div class="swiper-wrapper">

          <div class="swiper-slide">
            <div data-anim-child="slide-left delay-1" class="testimonials -type-3 sm:px-20 sm:py-40 bg-white">
              <div class="row y-gap-30 md:text-center md:justify-center">
                  {{-- <div class="col-md-auto">
                    <div class="testimonials__image">
                      <img src="img/home-4/testimonials/1.png" alt="image">
                    </div>
                  </div> --}}

                <div class="col-md">
                  {{-- <div class="d-flex items-center md:justify-center">
                    <span class="text-14 lh-1 text-yellow-1">4.5</span>
                    <div class="x-gap-5 px-10">
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                    </div>
                    <span class="text-13 lh-1">(1991)</span>
                  </div> --}}

                  <p class="testimonials__text text-16 lh-17 fw-500 mt-15">Novatio kompaniyasining bilim bo'yicha yetakchi xodimasi.O'z ishining ustasi.Tabobat ilmining egasi</p>

                  <div class="mt-15">
                    <div class="text-15 lh-1 text-dark-1 fw-500">Muhlisa H</div>
                    <div class="text-13 lh-1 mt-10">Doctor</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-slide">
            <div data-anim-child="slide-left delay-2" class="testimonials -type-3 sm:px-20 sm:py-40 bg-white">
              <div class="row y-gap-30 md:text-center md:justify-center">
                {{-- <div class="col-md-auto">
                  <div class="testimonials__image">
                    <img src="img/home-4/testimonials/2.png" alt="image">
                  </div>
                </div> --}}

                <div class="col-md">
                  {{-- <div class="d-flex items-center md:justify-center">
                    <span class="text-14 lh-1 text-yellow-1">4.5</span>
                    <div class="x-gap-5 px-10">
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                    </div>
                    <span class="text-13 lh-1">(1991)</span>
                  </div> --}}

                  <p class="testimonials__text text-16 lh-17 fw-500 mt-15">Kompaniyamizning asosiy ustunlaridan biri marketolog, sales doctor, designer, digital bo'lim boshlig'i</p>

                  <div class="mt-15">
                    <div class="text-15 lh-1 text-dark-1 fw-500">Botir B</div>
                    <div class="text-13 lh-1 mt-10">Marketolog</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-slide">
            <div data-anim-child="slide-left delay-3" class="testimonials -type-3 sm:px-20 sm:py-40 bg-white">
              <div class="row y-gap-30 md:text-center md:justify-center">
                {{-- <div class="col-md-auto">
                  <div class="testimonials__image">
                    <img src="img/home-4/testimonials/3.png" alt="image">
                  </div>
                </div> --}}

                <div class="col-md">
                  {{-- <div class="d-flex items-center md:justify-center">
                    <span class="text-14 lh-1 text-yellow-1">4.5</span>
                    <div class="x-gap-5 px-10">
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                      <i class="text-11 icon-star text-yellow-1"></i>
                    </div>
                    <span class="text-13 lh-1">(1991)</span>
                  </div> --}}

                  <p class="testimonials__text text-16 lh-17 fw-500 mt-15">Kompaniyaning informatsion texnologiyalar bo'limi yetakchisi.Rekrut bo'lim boshlig'i.Kompaniyaning asosiy dasturchisi</p>

                  <div class="mt-15">
                    <div class="text-15 lh-1 text-dark-1 fw-500">To'lqin E</div>
                    <div class="text-13 lh-1 mt-10">Dasturchi</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="d-flex justify-center x-gap-15 items-center pt-60 lg:pt-40">
          <div class="col-auto">
            <button class="d-flex items-center text-24 arrow-left-hover js-prev">
              <i class="icon text-white icon-arrow-left"></i>
            </button>
          </div>
          <div class="col-auto">
            <div class="pagination -arrows js-pagination"></div>
          </div>
          <div class="col-auto">
            <button class="d-flex items-center text-24 arrow-right-hover js-next">
              <i class="icon text-white icon-arrow-right"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="m-4">
    <div class="row justify-center text-center">
      <div class="col-auto">

        <div class="sectionTitle ">

          <h2 class="sectionTitle__title text-dark">Reyting</h2>

        </div>

      </div>
    </div>
    <div class="py-30">
      <div class="col-lg-12">
          <table class="table w-1/1">
              <thead>
                  <tr>
                      <th>FIO</th>
                      <th>Viloyat</th>
                      <th>Ball</th>
                  </tr>
              </thead>
              <tbody>
                @isset($ball)
                    @foreach ($ball as $item)
                    <tr>
                      <td>{{$item['f']}} {{$item['l']}}</td>
                      <td>{{$item['r']}}</td>
                      <td>{{$item['b']}}</td>
                      
                    </tr>
                    @endforeach
                @endisset

              </tbody>
          </table>
      </div>                                   

  </div>
  </section>
  {{-- <section class="layout-pt-sm layout-pb-sm bg-light-6">
    <div class="container">
      <div class="row y-gap-30 justify-between sm:justify-start items-center">

        <div class="col-lg-auto col-md-2 col-sm-3 col-4">
          <div class="d-flex justify-center items-center px-4">
            <img class="w-1/1" src="img/clients/1.svg" alt="clients image">
          </div>
        </div>

        <div class="col-lg-auto col-md-2 col-sm-3 col-4">
          <div class="d-flex justify-center items-center px-4">
            <img class="w-1/1" src="img/clients/2.svg" alt="clients image">
          </div>
        </div>

        <div class="col-lg-auto col-md-2 col-sm-3 col-4">
          <div class="d-flex justify-center items-center px-4">
            <img class="w-1/1" src="img/clients/3.svg" alt="clients image">
          </div>
        </div>

        <div class="col-lg-auto col-md-2 col-sm-3 col-4">
          <div class="d-flex justify-center items-center px-4">
            <img class="w-1/1" src="img/clients/4.svg" alt="clients image">
          </div>
        </div>

        <div class="col-lg-auto col-md-2 col-sm-3 col-4">
          <div class="d-flex justify-center items-center px-4">
            <img class="w-1/1" src="img/clients/5.svg" alt="clients image">
          </div>
        </div>

        <div class="col-lg-auto col-md-2 col-sm-3 col-4">
          <div class="d-flex justify-center items-center px-4">
            <img class="w-1/1" src="img/clients/6.svg" alt="clients image">
          </div>
        </div>

      </div>
    </div>
  </section> --}}
@endsection