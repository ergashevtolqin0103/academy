<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('partials.css')
</head>
<body class="preloader-visible" data-barba="wrapper">

    <!-- preloader start -->
    <div class="preloader js-preloader">
      <div class="preloader__bg"></div>
    </div>
    <!-- preloader end -->
  
    <!-- barba container start -->
    <div class="barba-container" data-barba="container">
  
  
      <main class="main-content  ">
        @if (isset(Auth::user()->status))
            @if (Auth::user()->status == 1 || Auth::user()->status == 3)
            @include('components.header-nomzod')
          @else
            @include('components.header')
          @endif
        @else
            
        @endif

        

  
        <div class="content-wrapper  js-content-wrapper">
  
  
          @yield('content')
  
  
        {{-- @include('components.footer') --}}
  
  
        </div>
      </main>
    </div>
    <!-- barba container end -->
  
    <!-- JavaScript -->
    @include('partials.js')
    @yield('script')
  
  </body>
</html>
