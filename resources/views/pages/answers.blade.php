@extends('layouts.app')
@section('content')

  <section class="layout-pb-lg">
    <div class="container" style="margin-top:100px;">
      <div class="row y-gap-50">
        <div class="col-lg-12">
          <table class="table w-1/1">
            <thead>
              <tr>
                <th>Dars</th>
                <th>Test soni</th>
                <th>Togri javob</th>
                <th>Natija</th>
                <th>Ko'rish</th>
              </tr>
            </thead>
            <tbody>

              @foreach ($answers as $item)
                  
              <tr>
                <td>{{$item['lesson']->title}}</td>
                <td>{{$item['count']}}</td>
                <td>{{$item['ans']}}</td>
                <td>{{floor($item['ans']*100/$item['count'])}}%</td>
                <td>
                  <a href="{{route('result.test',$item['lesson']->id)}}">
                    <span class="material-symbols-outlined" style="color:green">
                      visibility
                      </span>
                  </a>
                </td>
              </tr>

              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>

@endsection
@section('script')

@endsection
