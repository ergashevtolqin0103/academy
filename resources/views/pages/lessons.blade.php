@extends('layouts.app')
@section('content')
<aside class="lesson-sidebar bg-light-3" >
    <div class="px-30 sm:px-20">
      <div class="accordion -block-2 text-left js-accordion">

        @foreach ($course as $item)
            

        <div class="accordion__item mb-2">
          <div class="accordion__button py-20 px-30 text-center">
            {{-- <div class="d-flex items-center "> --}}
              <span class="text-17 fw-500 text-dark-1">{{$item->title}} kursi</span>
            {{-- </div> --}}
          </div>
        </div>

          <div class="card" style="background-color: #FFFFFF;
          border-radius: 16px;
          padding: 0;
          border: 1px solid #EDEDED">
            <div class="accordion__content__inner px-30 py-30">
              <div class="y-gap-30">

              @foreach ($item->lesson as $k => $les)

                <div class="">
                  <div class="d-flex">
                    <div class="d-flex justify-center items-center size-30 rounded-full bg-purple-3 mr-10">
                      <div class="icon-play text-9"></div>
                    </div>

                    <div class="">
                      <div>{{$les->title}}</div>
                      <div class="d-flex x-gap-20 items-center pt-5">
                        {{-- @if ($les->check_test == 1 || $ids->id == $les->id) --}}
                        @if (in_array($les->id,$checks) || $ids->id == $les->id)
                          <a href="{{route('lessons',$les->id)}}" class="text-14 lh-1 text-purple-1 underline">Darsni ko'rish</a>
                        @else
                          <span class="material-symbols-outlined">
                            lock
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>

                </div>
                
              @endforeach

              </div>
            </div>
          </div>

        @endforeach

      </div>
    </div>
  </aside>

  <section class="layout-pt-lg layout-pb-lg lg:pt-40">
    <div class="container">
      <div class="row justify-end">
        <div class="col-xxl-8 col-xl-7 col-lg-8">
          <div class="relative pt-40">
            <h3 class="mb-5 col-12 d-flex flex-column items-center">{{$lesson->title}} darsi</h3>

            <iframe style="width: 100%;height: 30%;"  src="https://www.youtube.com/embed/{{$lesson->video_link}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            {{-- <img class="w-1/1 rounded-16" src="img/lesson-single/1.png" alt="image">
            <div class="absolute-full-center d-flex justify-center items-center">
              <a href="#" class="d-flex justify-center items-center size-60 rounded-full bg-white">
                <div class="icon-play text-18"></div>
              </a>
            </div> --}}
          </div>

          <div class="mt-60 lg:mt-40">
            <h4 class="text-18 fw-500">Izoh</h4>
            <p class="mt-30">
              {{$lesson->description}}
            </p>
            {{-- <button class="text-purple-1 fw-500 underline mt-30">Show more</button> --}}
            <div class="col-12 d-flex flex-column items-center" style="margin-top: 30px;">
                <a href="{{route('begin.test',$lesson->id)}}">
                  <button type="submit" name="submit" id="submit" class="button -md -purple-1 text-white">
                    Test yechish
                  </button>
                </a> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')

@endsection
