
@extends('admin.layouts.app')
@section('admin-content')
    <div class="dashboard__content bg-light-4 pt-1">

        <div class="row y-gap-30">
            <div class="col-12">
                <div class="rounded-16 bg-white shadow-4 h-100">
                    {{-- <div class="d-flex items-center py-20 px-30 border-bottom-light">
              <h2 class="text-17 lh-1 fw-500">Page Head</h2>
            </div> --}}
                    <div class="d-flex justify-between items-center py-20 px-30 border-bottom-light">
                        <h2 class="text-17 lh-1 fw-500">Nomzodlar ro'yhati</h2>
                        {{-- <div>
                            <a href="{{ route('course.create') }}" class="button h-50 px-30 -purple-1 text-white">Yangi
                                test qo'shish</a>
                        </div> --}}
                    </div>
                    <div class="py-30 px-30">
                        <div class="col-lg-12">
                            <table class="table w-1/1">
                                <thead>
                                    <tr>
                                        <th>FIO</th>
                                        <th>Izoh</th>
                                        <th>Yosh</th>
                                        <th>Viloyat</th>
                                        <th>Tuman</th>
                                        <th>Guruh</th>
                                        <th>Birth</th>
                                        <th>Saqlash</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @isset($rekruts)
                                      @foreach ($rekruts as $item)
                                      <form action="{{route('nomzod.update',$item->id)}}" method="post">
                                        @csrf
                                        <tr>
                                            <td>{{$item->full_name}} {{$item->last_name}}</td>
                                            <td>{{$item->phone}}</td>
                                            <td>{{$item->age}}</td>
                                            <td>{{$item->region->name}}</td>
                                            <td>{{$item->district->name}}</td>
                                            <td>{{$item->group->title}}</td>
                                            <td>
                                                @if($item->user == null)
                                                <input type="date" name="birthday">

                                                @else
                                                {{$item->user->birthday}}
                                                
                                                @endif
                                                
                                            </td>
                                            <td> 
                                                @if($item->user == null)

                                                <button type="submit" class="btn btn-primary -purple-1">
                                                    <i class="fas fa-save" ></i>
                                                </button>
                                                
                                                @else
                                                
                                                
                                                @endif
                                                
                                            </td>
                                            
                                        </tr>
                                      </form>
                                      @endforeach
                                  @endisset

                                </tbody>
                            </table>
                        </div>                                   

                        {{-- @include('components.paginate',['pagination_v' => $courses]) --}}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('admin-script')
@endsection
