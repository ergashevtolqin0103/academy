@extends('layouts.app')
@section('content')


  <section class="layout-pt-lg layout-pb-lg lg:pt-40">
    <div class="container">
      <div class="row justify-end">
        <div class="col-xxl-8 col-xl-7 col-lg-8">
          <div class="relative pt-40">
            <h3 class="mb-5 col-12 d-flex flex-column items-center">{{$lesson->title}} darsi</h3>

            <iframe style="width: 100%;height: 50%;"  src="https://www.youtube.com/embed/{{$lesson->video_link}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            {{-- <img class="w-1/1 rounded-16" src="img/lesson-single/1.png" alt="image">
            <div class="absolute-full-center d-flex justify-center items-center">
              <a href="#" class="d-flex justify-center items-center size-60 rounded-full bg-white">
                <div class="icon-play text-18"></div>
              </a>
            </div> --}}
          </div>

          <div class="mt-60 lg:mt-40">
            <h4 class="text-18 fw-500">Izoh</h4>
            <p class="mt-30">
              {{$lesson->description}}
            </p>
            {{-- <button class="text-purple-1 fw-500 underline mt-30">Show more</button> --}}
            <div class="col-12 d-flex flex-column items-center" style="margin-top: 30px;">
                <a href="{{route('begin.test',['lesson_id' => $lesson->id,'rekrut_id' => $rekrut->id])}}">
                  <button type="submit" name="submit" id="submit" class="button -md -purple-1 text-white">
                    Test yechish
                  </button>
                </a> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')

@endsection
