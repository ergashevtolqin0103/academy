@extends('layouts.app')
@section('content')
    <section data-anim-wrap class="masthead -type-4">
        <div class="container">
            <form action="" method="POST">
                @csrf
                <div class="row y-gap-30">
                    <div class="col-12 d-flex flex-column items-center">
                        @if ($protsent > 50)
                            <h3 style="color:green">Natija {{$protsent}}% siz o'tdingiz</h3>
                            
                        @else
                            <h3 style="color:red">Natijala {{$protsent}}% siz o'tolmadingiz</h3>
                            
                        @endif
                    </div>
                    <div class="col-xl-9">
                        <div class="rounded-16 bg-white -dark-bg-dark-1 shadow-4">
                            {{-- <div class="d-flex items-center py-20 px-30 border-bottom-light">
                <h2 class="text-17 lh-1 fw-500">Quiz</h2>
                <div id="counter">19</div>
                </div> --}}

                            {{-- <div class="py-10 px-10" id="style-1"> --}}
                            <div class="py-10 px-10">
                                @isset($results)
                                    @foreach ($results as $key => $item)
                                        <div class="border-light overflow-hidden rounded-8 mb-20">

                                            <div class="py-40 px-40 bg-dark-5">
                                                <div class="d-flex justify-between">
                                                    <h4 class="text-18 lh-1 fw-500 text-white">Savol {{ $key + 1 }}</h4>
                                                    <div class="d-flex x-gap-50">
                                                    </div>
                                                </div>
                                                

                                                <div class="text-20 lh-1 text-white mt-15">{{ $item->question->question }}</div>
                                            </div>

                                            @php
                                                $arr = ['A','B','C','D'];
                                            @endphp
                                            <div class="d-flex pt-15 pb-15 px-40">
                                                <div class="text-success">
                                                    <span class="badge badge-danger" >
                                                        Togri javob {{$item->question->answer}} 
                                                        @if($item->answer == $item->question->answer) 
                                                        <i class="fas fa-check"></i>

                                                        @else
                                                        <i class="fas fa-close"></i>

                                                        @endif
                                                        {{-- <i class="fas fa-close" style="background: @if($item->answer == $item->question->answer) #05c43e87 @else #fe0b0b85 @endif"></i> --}}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="px-40 py-40" >

                                                @foreach ($arr as $letter)
                                                
                                                    @if ($letter == $item->answer)
                                                    
                                                    <div class="form-radio d-flex items-center ">
                                                        <div class="radio">
                                                            <input type="radio" class="radioBtnClass" name="{{ $item->id }}"
                                                                value="{{$letter}}" checked>
                                                            <div class="radio__mark">
                                                                <div class="radio__icon"></div>
                                                            </div>
                                                        </div>
                                                        @php
                                                            $d = 'answer_'.strtolower($letter);
                                                        @endphp
                                                        <div class="fw-500 ml-12">{{$letter}}. {{ $item->question->$d }}</div>
                                                    </div>
                                                    @else
                                                    <div class="form-radio d-flex items-center ">
                                                        <div class="radio">
                                                            <input type="radio" class="radioBtnClass" name="{{ $item->id }}"
                                                                value="{{$letter}}" disabled>
                                                            <div class="radio__mark">
                                                                <div class="radio__icon"></div>
                                                            </div>
                                                        </div>
                                                        @php
                                                            $d = 'answer_'.strtolower($letter);
                                                        @endphp
                                                        <div class="fw-500 ml-12">{{$letter}}. {{ $item->question->$d }}</div>
                                                    </div>
                                                    @endif
                                                @endforeach

                                                


                                                {{-- <div class="form-radio d-flex items-center mt-20">
                                                    <div class="radio">
                                                        <input type="radio" class="radioBtnClass" name="{{ $item->id }}"
                                                            value="B" required>
                                                        <div class="radio__mark">
                                                            <div class="radio__icon"></div>
                                                        </div>
                                                    </div>
                                                    <div class="fw-500 ml-12">B. {{ $item->question->answer_b }}</div>
                                                </div><div class="form-radio d-flex items-center mt-20"> --}}
                                                    {{-- <div class="radio">
                                                        <input type="radio" class="radioBtnClass" name="{{ $item->id }}"
                                                            value="B" required>
                                                        <div class="radio__mark">
                                                            <div class="radio__icon"></div>
                                                        </div>
                                                    </div>
                                                    <div class="fw-500 ml-12">B. {{ $item->question->answer_b }}</div>
                                                </div> --}}


                                                {{-- <div class="form-radio d-flex items-center mt-20">
                                                    <div class="radio">
                                                        <input type="radio" class="radioBtnClass" name="{{ $item->id }}"
                                                            value="C" required>
                                                        <div class="radio__mark">
                                                            <div class="radio__icon"></div>
                                                        </div>
                                                    </div>
                                                    <div class="fw-500 ml-12">C. {{ $item->question->answer_c }}</div>
                                                </div>

                                                <div class="form-radio d-flex items-center mt-20">
                                                    <div class="radio">
                                                        <input type="radio" class="radioBtnClass" name="{{ $item->id }}"
                                                            value="D" required>
                                                        <div class="radio__mark">
                                                            <div class="radio__icon"></div>
                                                        </div>
                                                    </div>
                                                    <div class="fw-500 ml-12">D. {{ $item->question->answer_d }}</div>
                                                </div> --}}

                                            </div>
                                        </div>
                                    @endforeach
                                @endisset

                                {{-- <div class="d-flex justify-between items-center mt-40">
                                    <button href="button" class="button -icon -purple-3 h-50 text-purple-1">
                                        Oldingisi
                                    </button>

                                    <button type="button" class="button -icon -purple-3 h-50 text-purple-1">
                                        Keyingisi
                                    </button>
                                </div> --}}
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3">
                        <div class="row y-gap-30">
                            <div class="col-12">
                                <div class="pt-20 pb-30 px-30 rounded-16 bg-white -dark-bg-dark-1 shadow-4">
                                    <h5 class="text-17 fw-500 mb-30">Natija</h5>

                                    <div class="d-flex items-center">
                                        <div class="progress-bar w-1/1">
                                            <div class="progress-bar__bg bg-light-3"></div>
                                            <div class="progress-bar__bar bg-purple-1 for_test__bar" style="width: {{$protsent}}% !important"></div>
                                            <div class="progress-bar__bar bg-purple-1" style="width: % !important"></div>
                                        </div>

                                        <div class="d-flex y-gap-10 justify-between items-center ml-15">
                                            <div class="for_test__prot">{{$protsent}}%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="pt-20 pb-30 px-30 rounded-16 bg-white -dark-bg-dark-1 shadow-4">
                                    <h5 class="text-17 fw-500 mb-30">Jami {{count($results)}} ta test</h5>

                                    {{-- <div class="row x-gap-10 y-gap-10"> --}}
                                        {{-- @isset($tests) --}}
                                            {{-- @foreach ($tests as $key => $item) --}}
                                                {{-- <div class="col-auto">
                                                    <a href="#"
                                                        class="button -single-icon -light-3 text-dark-1 p-2 rounded-8">
                                                        <div class="text-15 lh-1 fw-500">{{ $tests->currentPage() }} - savol</div>
                                                    </a>
                                                </div> --}}
                                            {{-- @endforeach --}}
                                        {{-- @endisset --}}

                                    {{-- </div> --}}
{{-- 
                                    <button type="submit"
                                        class="button -md -dark-1 text-white -dark-button-white mt-30 for_test__button">Keyingisi</button>
                                    <button type="button"
                                        class="button -md -dark-1 text-white -dark-button-white mt-30 for_test__button_none d-none">Keyingisi</button> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        @endsection
        @section('script')
            <script>

                


                $(function() {
                    $(".radioBtnClass").click(function() {
                        var all_count = 0;
                        var check_count = 0;
                        $('input:radio').each(function() {
                            all_count += 1;
                        });
                        $('input:radio:checked').each(function() {
                            check_count += 1;
                        });

                        var protsent = (check_count * 100) / (all_count / 4);
                        let bar = document.querySelector(".for_test__bar");
                        bar.style.width = protsent + "%";
                        $('.for_test__prot').text(Math.floor(protsent) + '%');

                        $('.for_test__button').click(function() {
                            $('.for_test__button').addClass('d-none');
                            $('.for_test__button_none').removeClass('d-none');
                        });


                    });



                });
            </script>
        @endsection
