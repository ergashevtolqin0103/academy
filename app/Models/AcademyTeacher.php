<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademyTeacher extends Model
{
    use HasFactory;

    protected $guarded = [];

}
