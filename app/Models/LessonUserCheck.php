<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonUserCheck extends Model
{
    use HasFactory;

    protected $table = 'lesson_user_checks';

    protected $guarded = [];

    // public function lesson()
    // {
    //     return $this->hasMany(Lesson::class,'course_id','id');
    // }
}
