<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademyUser extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function rekrut()
    {
        return $this->belongsTo(Rekrut::class,'rekrut_id', 'id');
    }
}
