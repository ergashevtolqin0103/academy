<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rekrut extends Model
{
    use HasFactory;

    protected $table = 'rekruts';

    protected $guarded = [];

    public function region()
    {
        return $this->belongsTo(Region::class,'region_id', 'id');
    }

    public function district()
    {
        return $this->belongsTo(District::class,'district_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(RekrutGroup::class,'group_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(AcademyUser::class,'rekrut_id', 'id');
    }
}
