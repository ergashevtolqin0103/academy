<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnswerTest extends Model
{
    use HasFactory;

    protected $table = 'academy_answer';

    protected $fillable = [
        'test_id',
        'user_id',
        'answer',
        'check'
    ];

    public function question()
    {
        return $this->belongsTo(OpenTest::class,'test_id', 'id');
    }
}
