<?php

namespace App\Http\Controllers;

use App\Models\AcademyStudentBall;
use App\Models\AcademyUser;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\Rekrut;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     

    public function authPhoneNumber($phone)
    {
        $rekrut = Rekrut::where('phone','+'.$phone)->first();

        $course = Course::with(['lesson' => function($q){
            $q->orderBy('sort','ASC');
        }])->orderBy('id','ASC')->limit(1)->get();

        $lesson = Lesson::where('course_id',$course[0]->id)->orderBy('sort','ASC')->first();

        
        $ids = Lesson::where('check_test',0)->orderBy('sort','ASC')->first();

        return view('pages.tests',[
            'course' => $course,
            'lesson' => $lesson,
            'ids' => $ids,
            'rekrut' => $rekrut,
        ]);

    }
    public function index()
    {
        $users = AcademyUser::with('rekrut')->where('status',1)->get();


        // dd($users[0]->rekrut);
        $ball = [];

        foreach ($users as $key => $value) {
            if($value->rekrut->group_id == 2)
            {
                $reyting = AcademyStudentBall::where('user_id',$value->id)
                ->sum('ball');
    
                $ball[] = array('f' => $value->rekrut->full_name??null,'l' => $value->rekrut->last_name??null,'r' => $value->rekrut->region->name??null,'b' => $reyting);
     
            }
           
        }

        array_multisort(array_column($ball, 'b'), SORT_DESC, $ball);


        return view('index',compact('ball'));
    }
}
