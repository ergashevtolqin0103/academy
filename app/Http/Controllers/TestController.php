<?php

namespace App\Http\Controllers;

use App\Models\AnswerTest;
use App\Models\Lesson;
use App\Models\LessonUserCheck;
use App\Models\OpenTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    public function index()
    {
        
    }

    public function testSolving($lesson_id,$rekrut_id)
    {
        // return $lesson_id;
        $lesson = Lesson::find($lesson_id);

        $test_ids = OpenTest::where('lesson_id',$lesson_id)->pluck('id')->toArray();

        $answers = AnswerTest::whereIn('test_id',$test_ids)->whereDate('created_at',date('Y-m-d'))->where('user_id',$rekrut_id)->pluck('test_id')->toArray();
        
        // $tests = OpenTest::where('lesson_id',$lesson_id)->whereNotIn('id',$answers)->orderBy('id')->get();
        $tests = OpenTest::where('lesson_id',$lesson_id)->orderBy('id')->get();

        $count = OpenTest::where('lesson_id',$lesson_id)->count();


        // $protsent = floor(count($answers)*100/$count);


        if(count($answers) == $count)
        {
            return redirect()->route('result.test',['lesson_id' => $lesson_id,'rekrut_id' => $rekrut_id]);
        }
        return view('pages.begin-test',[
            'tests' => $tests,
            'lesson' => $lesson,
            'rekrut_id' => $rekrut_id, 
            // 'question_number' => count($answers),
            'all_q_number' => $count,
        ]);
    }

    public function resultTest($lesson_id,$rekrut_id)
    {
        $user_id = $rekrut_id;

        $lesson = Lesson::find($lesson_id);

        $test_ids = OpenTest::where('lesson_id',$lesson_id)->pluck('id')->toArray();

        $results = AnswerTest::where('user_id',$user_id)->whereIn('test_id',$test_ids)->get();

        $checks = AnswerTest::where('user_id',$user_id)->whereIn('test_id',$test_ids)->where('check',1)->count();
        
        $count = OpenTest::where('lesson_id',$lesson_id)->count();

        $protsent = floor(($checks)*100/$count);

        // return $protsent;
        return view('pages.result-test',[
            'results' => $results,
            'protsent' => $protsent,
            'lesson' => $lesson,
        ]);

    }

    public function endTest(Request $request,$lesson_id)
    {
        $inputs = $request->all();
        $rid = $inputs['rid'];
        unset($inputs['_token']);
        unset($inputs['rid']);
        // return $inputs;
        // $user_id = Auth::id();

        // return $inputs;

        foreach ($inputs as $id => $answer) {
            $open_test = OpenTest::find($id);
            $check = ($answer == $open_test->answer) ? 1 : 0;
            $new = new AnswerTest([
                'user_id' => $rid,
                'test_id' => $id,
                'answer' => $answer,
                'check' => $check
            ]);
            $new->save();
        }
        
        // $checks = new LessonUserCheck;
        // $checks->user_id = Auth::id();
        // $checks->lesson_id = $lesson_id;
        // $checks->check = 1;
        // $checks->save();

        // $lesson = Lesson::where('id',$lesson_id)->update([
        //     'check_test' => 1,
        // ]);

        return redirect()->route('result.test',['lesson_id' => $lesson_id,'rekrut_id' => $rid]);
    }

    public function answers()
    {
        $lessons = Lesson::all();

        $answers = [];
        foreach ($lessons as $key => $value) {
            $tests = OpenTest::where('lesson_id',$value->id)->pluck('id')->toArray();

            $ans = AnswerTest::whereIn('test_id',$tests)->where('user_id',Auth::id())->where('check',1)->count();

            $answers[] = array(
                'lesson' => $value,
                'count' => count($tests),
                'ans' => $ans,
            );

        }
        return view('pages.answers',[
            'answers' => $answers
        ]);
    }
}
