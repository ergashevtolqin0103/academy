<?php

namespace App\Http\Controllers;

use App\Models\AcademyCategoryBall;
use App\Models\AcademyStudentBall;
use App\Models\AcademyUser;
use App\Models\Rekrut;
use App\Models\RekrutGroup;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class NomzodController extends Controller
{
    public function list()
    {
        $groups = RekrutGroup::orderBy('id','DESC')->first();

        $rekruts = Rekrut::with('user')->where('group_id',2)->where('xolat',3)->get();

        // dd($rekruts);

        return view('pages.nomzod.list',[
            'rekruts' => $rekruts
        ]);
        
    }

    public function reg()
    {
        $groups = RekrutGroup::orderBy('id','ASC')->first();

        $rekruts = Rekrut::with('user')->where('xolat',3)->get();
        // $rekruts = Rekrut::with('user')->whereIn('xolat',[1,2,3,4,5,6,7])->get();

        $ustoz = DB::table('tg_user')->orderBy('first_name','ASC')->get();
        $pharmacy = DB::table('tg_pharmacy')->orderBy('name','ASC')->get();

        // dd($rekruts);

        return view('pages.nomzod.reg',[
            'rekruts' => $rekruts,
            'ustoz' => $ustoz,
            'pharmacy' => $pharmacy,
        ]);
        
    }

    public function regis(Request $request,$id)
    {

        $rekruts = Rekrut::find($id);

        $academy = AcademyUser::where('rekrut_id',$id)->first();
        
        $new = DB::table('tg_user')->insertGetId([
            'password' => Hash::make($academy->parol),
            'last_login' => NULL,
            'is_superuser' => FALSE,
            'username' => $academy->username,
            'first_name' => $rekruts->full_name,
            'last_name' => $rekruts->last_name,
            'phone_number' => $rekruts->phone,
            'is_staff' => FALSE,
            'is_active' => TRUE,
            'is_verified' => TRUE,
            'date_joined' => date('Y-m-d'),
            'district_id' => $rekruts->district_id,
            'region_id' => $rekruts->region_id,
            'specialty_id' => 1,
            'email' => NULL,
            'tg_id' => 990821015,
            'birthday' => $academy->birthday,
            'admin' => FALSE,
            'write_time' => date('Y-m-d'),
            'salary' => 1500000,
            'pr' => $academy->parol,
            'tg_file_id' => NULL,
            'rol_id' => 27,
            'last_seen' => NULL,
            'teacher' => FALSE,
            'image_change' => TRUE,
            'pharmacy_id' => NULL,
            'image_url' => 'https://telegra.ph//file/04f99aa16eebd4af2a42c.jpg',
            'status' => 0,
            'level' => 0,
            'rm' => 0,
            'first_enter' => 0,

        ]);        

        DB::table('daily_works')->insertGetId([
            'user_id' => $new,
            'start_work' => '09:00:00',
            'finish_work' => '18:00:00',
            'start' => '2023-03-15'
        ]);

        DB::table('tg_pharmacy_users')->insertGetId([
            'user_id' => $new,
            'pharma_id' => $request->apteka,
        ]);

        DB::table('teacher_users')->insertGetId([
                'teacher_id' => $request->ustoz,
                'user_id' => $new,
                'week_date' => '2023-10-09',
            ]);

        $response = Http::post('notify.eskiz.uz/api/auth/login', [
            'email' => 'mubashirov2002@gmail.com',
            'password' => 'PM4g0AWXQxRg0cQ2h4Rmn7Ysoi7IuzyMyJ76GuJa'
        ]);
        $token = $response['data']['token'];

        $sms = Http::withToken($token)->post('notify.eskiz.uz/api/message/sms/send', [
            'mobile_phone' => substr($rekruts->phone,1),
            'message' => 'Login   ' . $academy->username  . '   Parol   '. $academy->parol .' .  Saytga kirish uchun link ustiga bosing  jang.novatio.uz/login',
            'from' => '4546',
            'callback_url' => 'http://0000.uz/test.php'
        ]);

        return redirect()->back();
        
    }
    public function update(Request $request,$id)
    {
        $nomzod = Rekrut::find($id);

        $username = 'nvt'.rand(100,999);
        $parol = rand(1000,9999);

        $user = new AcademyUser();
        $user->birthday = $request->birthday;
        $user->rekrut_id = $request->id;
        $user->parol = $parol;
        $user->username = $username;
        $user->password = Hash::make($parol);
        $user->save();


        $response = Http::post('notify.eskiz.uz/api/auth/login', [
            'email' => 'mubashirov2002@gmail.com',
            'password' => 'PM4g0AWXQxRg0cQ2h4Rmn7Ysoi7IuzyMyJ76GuJa'
        ]);
        $token = $response['data']['token'];

        $sms = Http::withToken($token)->post('notify.eskiz.uz/api/message/sms/send', [
            'mobile_phone' => substr($nomzod->phone,1),
            'message' => 'Login   ' . $username  . '   Parol   '. $parol .' .  Saytga kirish uchun link ustiga bosing  academy.novatio.uz',
            'from' => '4546',
            'callback_url' => 'http://0000.uz/test.php'
        ]);

        return redirect()->back();

    }

    public function viewNomzodlar()
    {
        $users = AcademyUser::with('rekrut')->where('status',1)->get();

        $category = AcademyCategoryBall::all();

        return view('admin.pages.nomzod.list',[
            'users' => $users,
            'category' => $category,
        ]);
    }

    public function ball(Request $request, $id)
    {
        $new = new AcademyStudentBall;
        $new->user_id = $id;
        $new->category = $request->xolat;
        $new->ball = $request->ball;
        $new->save();

        return redirect()->back();

    }
}
