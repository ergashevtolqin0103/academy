<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Lesson;
use App\Models\LessonUserCheck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::with('course')->orderBy('id')->paginate(5);

        return view('admin.pages.lesson.list',[
            'lessons' => $lessons,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $courses = Course::orderBy('id','ASC')->get();

        return view('admin.pages.lesson.create',[
            'courses' => $courses,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        // unset($inputs['_token']);

        $new_quiz = new Lesson($inputs);

        $new_quiz->save();
        
        return redirect()->back()->with('save_success','Ma\'lumotlar saqlandi.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lessons()
    {
        $course = Course::with(['lesson' => function($q){
            $q->orderBy('sort','ASC');
        }])->orderBy('id','ASC')->limit(1)->get();

        $lesson = Lesson::where('course_id',$course[0]->id)->orderBy('sort','ASC')->first();

        
        $ids = Lesson::where('check_test',0)->orderBy('sort','ASC')->first();

        return view('pages.lessons',[
            'course' => $course,
            'lesson' => $lesson,
            'ids' => $ids,
        ]);
    }

    public function viewLessons($id = null)
    {
        $course = Course::with(['lesson' => function($q){
            $q->orderBy('sort','ASC');
        }])->orderBy('id','ASC')->limit(1)->get();

        if($id)
        {
            $lesson = Lesson::find($id);
        }else{
            $lesson = Lesson::where('course_id',$course[0]->id)->orderBy('sort','ASC')->first();
        }

        $checks = LessonUserCheck::where('user_id',Auth::id())->pluck('lesson_id')->toArray();

        // return $checks;

        // $ids = Lesson::where('check_test',0)->orderBy('sort','ASC')->first();
        $ids = Lesson::whereNotIn('id',$checks)->orderBy('sort','ASC')->first();

        return view('pages.lessons',[
            'course' => $course,
            'lesson' => $lesson,
            'ids' => $ids,
            'checks' => $checks,
        ]);
    }
}
