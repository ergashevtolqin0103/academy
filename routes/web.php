<?php

use App\Http\Controllers\Admin\OpenTestController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\LessonController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NomzodController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/{phone}', [HomeController::class, 'authPhoneNumber'])->where('phone', '[0-9]+');

    Route::get('test/{lesson_id}/{rekrut_id}', [TestController::class, 'testSolving'])->name('begin.test');
    Route::post('end/{lesson_id}', [TestController::class, 'endTest'])->name('end.test');
    Route::get('result/{lesson_id}/{rekrut_id}', [TestController::class, 'resultTest'])->name('result.test');

Route::middleware('auth')->group(function () {

    // Route::get('/', function () {
    //     return view('index');
    // });
    
    Route::get('/', [HomeController::class, 'index']);

    
    Route::group(['prefix'=>'admin'], function(){
    
        Route::get('/', function () {
            return view('admin.layouts.app');
        })->name('admin');
    
        Route::get('nomzodlar', [NomzodController::class, 'viewNomzodlar'])->name('nomzodlar');

        Route::resource('teacher', TeacherController::class);
        Route::resource('course', CourseController::class);
        Route::resource('lesson', LessonController::class);
        Route::resource('open-test', OpenTestController::class);
    });
    
    Route::get('lessons/{id?}', [LessonController::class, 'viewLessons'])->name('lessons');

    Route::get('answers', [TestController::class, 'answers'])->name('answers');

    // Route::get('logout', [TestController::class, 'logout'])->name('logout');
    // Route::get('view-lesson/{id}', [LessonController::class, 'viewLessons'])->name('view-lesson');

    // Route::get('test/{lesson_id}/{rekrut_id}', [TestController::class, 'testSolving'])->name('begin.test');
    // Route::post('end/{lesson_id}', [TestController::class, 'endTest'])->name('end.test');
    // Route::get('result/{lesson_id}/{rekrut_id}', [TestController::class, 'resultTest'])->name('result.test');

    #nomzod-begin
    Route::get('nomzod-list', [NomzodController::class, 'list'])->name('nomzod.list');
    Route::get('nomzod-reg', [NomzodController::class, 'reg'])->name('nomzod.reg');
    Route::post('nomzod-update/{id}', [NomzodController::class, 'update'])->name('nomzod.update');
    
    Route::post('nomzod-regis/{id}', [NomzodController::class, 'regis'])->name('nomzod.regis');

    Route::post('nomzod-ball/{id}', [NomzodController::class, 'ball'])->name('nomzod.ball');


    // Route::get('reyting', [NomzodController::class, 'ball'])->name('reyting');
    #nomzod-end


});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
